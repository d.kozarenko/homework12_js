"use strict";

let slides = document.querySelectorAll(".image-to-show");
let currentSlide = 1;
let buttonStop = document.createElement("button");
let buttonResume = document.createElement("button");
let delay = 3000;
let seconds = delay / 1000 + 1;
let timeLeft = document.querySelector(".time-left");
buttonStop.addEventListener("click", stopClickingHandler);
buttonResume.addEventListener("click", resumeClickingHandler);

function stopClickingHandler() {
  clearTimeout(showTimer);
  clearTimeout(timer);
}
function resumeClickingHandler() {
  if (!slides[3].classList.contains("none")) {
    currentSlide = 4;
  } else {
    currentSlide -= 1;
  }
  clearTimeout(timer);
  seconds = delay / 1000 + 1;
  showTimer = setTimeout(textTimer, 0);
  timer = setTimeout(nextSlide, 0);
}

let showTimer = setTimeout(textTimer, 0);
let timer = setTimeout(nextSlide, 0);

buttonsCheck();

function nextSlide() {
  slidesHide();
  slides[currentSlide - 1].classList.remove("none");
  currentSlide++;
  timer = setTimeout(nextSlide, delay);
  slideCheck();
}
function textTimer() {
  timeLeft.textContent = --seconds;
  if (seconds === 0) {
    seconds = delay / 1000;
    timeLeft.textContent = seconds;
  }
  showTimer = setTimeout(textTimer, 1000);
}
function slidesHide() {
  slides.forEach((element) => {
    element.classList.add("none");
  });
}
function slideCheck() {
  if (currentSlide > slides.length) {
    currentSlide = 1;
  }
}
function buttonsCheck() {
  if (
    !document
      .getElementsByClassName("buttons-wrapper")[0]
      .classList.contains("buttons-active")
  ) {
    buttonsCreate();
  }
}
function buttonsCreate() {
  let buttonsBox = document.getElementsByClassName("buttons-wrapper")[0];
  buttonsBox.classList.add("buttons-active");
  buttonStop.textContent = "Прекратить";
  buttonResume.textContent = "Возобновить";
  buttonsBox.append(buttonStop);
  buttonsBox.append(buttonResume);
}
